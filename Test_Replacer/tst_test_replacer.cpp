#include <QtTest>
#include <../functions.h>
#include "tst_test_replacer.h"

bool compareVec(const QVector<float> &vec1, const QVector<float> &vec2) {
    if (vec1 == vec2)
        return true;
    return false;
}

Test_Replacer::Test_Replacer()
{

}

//пак тестов для функции findArithmeticMean - нахождение среднего арифметического
void Test_Replacer::test_case1()
{
    QVector<float> test_vec = {10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    QCOMPARE(15, findArithmeticMean(test_vec));

    test_vec = {0, 20};
    QCOMPARE(10, findArithmeticMean(test_vec));

    test_vec = {-1, -2, -3, -4, -5};
    QCOMPARE(-3, findArithmeticMean(test_vec));
}

//пак тестов для функции replace - замена всех элементов на нечётных позициях средним арифметическим последовательности
void Test_Replacer::test_case2()
{
    QVector<float> test_vec_before = {1, 2, 3};
    replace(test_vec_before);
    QVector<float> test_vec_after = {2, 2, 2};
    QCOMPARE(true, compareVec(test_vec_before, test_vec_after));

    test_vec_before = {1, 2, 3};
    replace(test_vec_before);
    test_vec_after = {3, 2, 3};
    QCOMPARE(false, compareVec(test_vec_before, test_vec_after));
}

//пак тестов для функции isOdd - проверка на нечётность
void Test_Replacer::test_case3()
{
    int test_num = 24;
    QCOMPARE(false, isOdd(test_num));

    test_num = 45;
    QCOMPARE(true, isOdd(test_num));
}
