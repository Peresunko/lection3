QT += testlib
QT -= gui

TARGET = tst_test_replacer
CONFIG += console testcase
CONFIG -= app_bundle

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    main.cpp \
    tst_test_replacer.cpp

SOURCES += ../*.cpp

HEADERS += \
    tst_test_replacer.h

HEADERS += ../*.h
