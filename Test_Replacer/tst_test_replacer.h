#ifndef TST_TEST_REPLACER_H
#define TST_TEST_REPLACER_H

#include <QtCore>
#include <QtTest/QtTest>


class Test_Replacer : public QObject
{
    Q_OBJECT

public:
    Test_Replacer();

private slots:
    void test_case1();
    void test_case2();
    void test_case3();

};

#endif // TST_TEST_REPLACER_H
