#include "functions.h"

void replace (QVector<float> &vec)
{
    float mean = 0;
	mean = findArithmeticMean(vec);
    for (int i = 0; i < vec.size(); ++i) {
        if (isOdd(i+1)) {
            vec[i] = mean;
        }
    }
}
