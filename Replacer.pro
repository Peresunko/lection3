QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    replace.cpp \
    findMean.cpp \
    isOdd.cpp

HEADERS += \
    functions.h
