#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QVector>

float findArithmeticMean (const QVector<float> &vec); //среднее арифметическое

bool isOdd (const int num); //проверка на нечётность

void replace (QVector<float> &vec); //замена элементов на нечётных позициях средним арифметическим

#endif // FUNCTIONS_H
